package digw.io.scale;

import android.content.Context;
import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import android.util.Log;

public class HandlerWorker extends Handler {
    private static final int COMMAND_INSTALL_LOCAL_UPDATE = 0;
    private Handler mResponseHandler;

    private ProgressListener mProgressListener;

    interface ProgressListener {
        void onWorkerProgress(int step);
    }

    HandlerWorker(Looper looper, Context context, Handler responseHandler) {
        super(looper);
        mResponseHandler = responseHandler;
    }

    @Override
    public void handleMessage(Message msg) {
        switch (msg.what) {
            case COMMAND_INSTALL_LOCAL_UPDATE:
                loop();
                break;
        }
    }

    public void startUpdate() {
        Message msg = new Message();
        msg.what = COMMAND_INSTALL_LOCAL_UPDATE;
        sendMessage(msg);
    }

    private void loop() {
        boolean mainThread = Looper.myLooper() == Looper.getMainLooper();
        for(int i=0; i < 100; i++) {
            final int step = i;
            Log.d("HandlerWorker", "is Main Thread: " + mainThread + " Step: " + i);
            mResponseHandler.post(new Runnable() {
                @Override
                public void run() {
                    mProgressListener.onWorkerProgress(step);
                }
            });
        }
    }

    public void setProgressListener(HandlerWorker.ProgressListener progressListener) {
        mProgressListener = progressListener;
    }
}
