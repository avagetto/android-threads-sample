package digw.io.scale;

import android.os.Handler;
import android.os.HandlerThread;
import android.os.Looper;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;

public class MainActivity extends AppCompatActivity implements HandlerWorker.ProgressListener {
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        HandlerThread handlerThread = new HandlerThread("HandlerWorker: work thread");
        handlerThread.start();
        Handler responseHandler = new Handler();
        HandlerWorker handlerWorker = new HandlerWorker(handlerThread.getLooper(),
                getApplicationContext(), responseHandler);
        handlerWorker.setProgressListener(this);
        handlerWorker.startUpdate();

        loop();
    }

    private void loop() {
        boolean mainThread = Looper.myLooper() == Looper.getMainLooper();
        for(int i=0; i < 100; i++) {
            Log.d("MainActivity", "is Main Thread: " + mainThread + " Step: " + i);
        }
    }

    @Override
    public void onWorkerProgress(int step) {
        boolean mainThread = Looper.myLooper() == Looper.getMainLooper();
        Log.d("onWorkerProgress", "is Main Thread: " + mainThread + " Step: " + step);
    }
}
